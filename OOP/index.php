<?php

require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$animal = new Animal ("shaun");
echo "Name :" . $animal->name . "<br>";
echo "legs :" . $animal->legs . "<br>";
echo "cold blooded :" . $animal->coldblooded. "<br>";

$frog = new frog("buduk");
echo "Name : " . $frog->name . "<br>";
echo "legs : " . $frog->legs . "<br>";
echo "cold blooded : " . $frog->coldblooded. "<br>";
echo "Yell : " . $frog->yell ."<br>";

$ape = new ape("Kera Sakti");
echo "Name : " . $ape->name . "<br>";
echo "legs : " . $ape->legs . "<br>";
echo "cold blooded : " . $ape->coldblooded. "<br>";
echo "Yell : " . $ape->yell ."<br>";


?>